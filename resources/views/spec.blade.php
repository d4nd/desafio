@extends('layouts.app')

@section('title', 'Especificação')

@section('lead')
Descrição do que o desafio deveria contemplar.
@endsection

@section('content')

      <div class="row justify-content-md-center">
 

        <div class="col-md-8 order-md-1">

<p>Fala Sílvio, tudo blz?</p>

Segue os itens para o desafio:

    <ul>
        <li><a href="https://getbootstrap.com/docs/4.0/getting-started/introduction/">Bootstrap</a> | <a href="https://youtu.be/5BED0R0G3ow">Curso</a></li>
        <li><a href="http://www.wampserver.com/en/">WAMP Server</a> | <a href="https://youtu.be/yj_KWo4pNzE">Como instalar</a></li>
        <li><a href="http://br.phptherightway.com/">PHP</a></li>
    </ul>

As atividades basicamente deverão ser:
    <ul>
        <li>Baixar e instalar o WAMP</li>
        <li>Baixar o Bootstrap</li>
        <li>Usar o bootstrap para criar um formulário com campos como: nome, e-mail, telefone e o que mais achar bacana</li>
        <li>Criar um banco de dados no MySQL</li>
        <li>Criar uma tabela para receber os dados daquele formulário</li>
        <li>Criar um arquivo .PHP para receber os dados do formulário e salvar eles no banco de dados</li>
        <li>Subir esses arquivos no GitHub ou outro similar para eu testar</li>
    </ul>

<p>Qualquer dúvida ou explicação que precise NÃO DEIXE DE ME PROCURAR.</p>

Abs!
@endsection