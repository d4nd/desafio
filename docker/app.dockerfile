FROM centos:7

EXPOSE 80
COPY docker/httpd.conf /etc/httpd/conf/httpd.conf

RUN rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm && \
    rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm && \
    yum update -y  && \
    yum install -y php71w php71w-mysqli php71w-intl php71w-devel php71w-xml php71w-pear php71w-ldap php71w-mbstring php71w-gd \
        curl gcc gd gd-devel git httpd libpng-devel make mod_ssl nano nodejs && \
    yum clean all && \
    cd /tmp && php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php && php composer-setup.php --install-dir=/bin --filename=composer && \
    sed -i 's/;date.timezone = */date.timezone = America\/Sao_Paulo/' /etc/php.ini && \
    sed -i 's/short_open_tag = Off/short_open_tag = On/' /etc/php.ini && \
    sed -i 's/memory_limit.*/memory_limit = 2048M/' /etc/php.ini && \
    rm -f /etc/httpd/conf.d/welcome.conf

CMD ["-D", "FOREGROUND"]
ENTRYPOINT ["/usr/sbin/httpd"]